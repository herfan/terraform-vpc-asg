provider "aws" {
  version = "~> 3.0"
}

module "vpc" {
    source = "terraform-aws-modules/vpc/aws"

name    = "herfan-vpc"
cidr    = "10.255.112.0/20"

azs = [
    "ap-southeast-1a",
    "ap-southeast-1b",
    "ap-southeast-1c",
  ]

public_subnets = ["10.255.125.0/24"]
private_subnets = ["10.255.114.0/24"]
enable_nat_gateway = true

}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  # Autoscaling group
  name = "herfan-asg"

  min_size                  = 2
  max_size                  = 5
  desired_capacity          = 2
  wait_for_capacity_timeout = 0
  health_check_type         = "EC2"
  vpc_zone_identifier       = module.vpc.private_subnets

  lt_name                = "herfan-launch-template"
  description            = "Launch template herfan"
  update_default_version = true
  use_lc    = true
  create_lc = true
  mixed_instances_policy = true

  image_id          = "ami-0d058fe428540cd89"
  instance_type     = "t2.medium"

}
